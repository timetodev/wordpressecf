|--------------------------------------------------------------------------------------|
|-------------------------------- FRANCAIS---------------------------------------------|
|--------------------------------------------------------------------------------------|

Bonjour, ce code est mis en place pour L'ecf wordpress avancée , l'objectif création de child thème avec modification du thème et ajouts de PHPCSTutoriel D'installation :

    1 - Cloner le répositorie : git clone https://framagit.org/timetodev/wordpressecf.git 
    2 - Modifier le fichier de configuration du wordpress wp-config.php 
    3 - Importer la Base de donnée de wordpress joint dans le dossier SQL-Back 
    4 - Modifier les lignes dans wp-option(dans la base de donnée ) siteurl && home
    5 - Modifier le thème dans le dossier /wp-content/twentyseventeen-child/ (style.css)
        
|--------------------------------------------------------------------------------------|
|-------------------------------- English ---------------------------------------------|
|--------------------------------------------------------------------------------------|


Hello, this code is set up for the advanced wordpress ecf, the goal of creating child theme with theme modification and additions of PHPCSTutorial Setup:

    1 - Cloning the repository: git clone https://framagit.org/timetodev/wordpressecf.git
    2 - Edit the wordpress configuration file wp-config.php
    3 - Import the wordpress database attached to the SQL-Back folder
    4 - Edit the lines in wp-option (in the database) siteurl && home
    5 - Change the theme in the folder / wp-content / twentyseventeen-child / (style.css)